const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const {verificaToken} = require('../middleware/authetication');
const Usuario = require('../models/usuario');

const fs = require('fs');
const path = require('path');

/*
function imagenUsuario(id, res, nombreArchivo){
    Usuario.findById(id, (err, usuarIoDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!usuarIoDB){
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario no encontrado"
                }
            });
        }

        let pathImage = path.resolve(__dirname, `../uploads/usuarios/${usuarIoDB.img}`);
        console.log(pathImage)
        if(fs.existsSync(pathImage)){
            fs.unlinkSync(pathImage)
        }

        usuarIoDB.img = nombreArchivo;

        usuarIoDB.save((err, usuarioGuardado)=>{
            res.json({
                ok: true,
                usuario: usuarioGuardado,
                img: nombreArchivo
            });
        });
    });
}

app.put("/upload/:tipo/:id", function(req, res){
    let tipo = req.params.tipo;
    let id = req.params.id;

    if(!req.files){
        return res.status(400).json({
            ok: false,
            err: {
                message: "No se seleccionó ningún archivo"
            }
        });
    }
    // validar tipo
    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "Las tipos permitidas son " + tiposValidos.join(', ')
            }
        });
    }
    //validacion
    let archivo = req.files.archivo;
    let nombreArchivoCortado = archivo.name.split('.');
    let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extencionesValidas = ["png", "jpg", "gif", "jpeg"];
    if (extencionesValidas.indexOf(extencion) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "Las extensiones permitidas son " + extencionesValidas.join(', '),
                ext: extencion
            }
        });
    }
    //cambiar nombre
    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extencion}`
    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        imagenUsuario(id, res, nombreArchivo);
    });
});
*/
app.get('/profile', verificaToken, async function (req, res) {
    let id = req.user._id
    let user = await Usuario.findById(id)
    res.render('profile', {
        user
    })
});

app.put('/updateProfile/:id', verificaToken, async function(req, res){
    let id = req.params.id;
    let img = req.files.avatar;
    let { nombre, email, nameImg } = req.body;
    let nombreArchivoCortado = img.name.split('.');
    let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extencionesValidas = ["png", "jpg", "jpeg"];
    if (extencionesValidas.indexOf(extencion) < 0) {
        res.redirect("/profile");
    } else {
        let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extencion}`
        img.mv(`public/assets/img/${nombreArchivo}`, (err) => {
            if (err) {
                res.redirect("/profile");
            }
        });
        let pathImage = path.resolve(__dirname, `../public/assets/img/${nameImg}`);
        console.log(pathImage)
        if(fs.existsSync(pathImage)){
            fs.unlinkSync(pathImage)
        }
        await Usuario.findByIdAndUpdate(req.params.id, { nombre: nombre, email: email, img: nombreArchivo });
        res.redirect("/profile");
    }
});

module.exports = app;