const express = require('express');
const Nota = require('../models/inventario');
const Usuario = require('../models/usuario');
const router = express.Router();
const {verificaToken} = require('../middleware/authetication');
const fs = require('fs');
const path = require('path');

router.get('/notas', verificaToken, async function(req, res){
    console.log(req.user)
    let error = [];
    const notas = await Nota.find({});
    const usersName = await Usuario.find({});
    res.render('tareas', {error, notas, usersName })
});

router.get('/editNote/:id', verificaToken, async function(req, res){
    let error = [];
    const usersName = await Usuario.find({});
    const nota = await Nota.findById(req.params.id);
    res.render('editNote', {error, nota, usersName })
});

router.post('/newNota', verificaToken,  async function(req, res) {
    let error = [];
    let ref = req.files.ref;
    let { titulo, descripcion, user } = req.body;
    let nombreArchivoCortado = ref.name.split('.');
    let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extencionesValidas = ["png", "jpg", "jpeg"];
    if (extencionesValidas.indexOf(extencion) < 0) {
        error.push({ text: "El archivo no es una imagen." });
    } 
    if (!titulo) {
        error.push({ text: "Título es requerido." });
    }
    if (!descripcion) {
        error.push({ text: "Descripción es requerido." });
    }
    if (error.length > 0) {
        res.render("tareas", {
            error,
        });
    } else {

        let nombreArchivo = `${titulo}-${new Date().getMilliseconds()}.${extencion}`
        ref.mv(`public/assets/uploads/${nombreArchivo}`, (err) => {
            if (err) {
                res.render("tareas", {
                    error: err,
                });
            }
        });

        let newNota = new Nota({ title: titulo, description: descripcion, user: user, ref: nombreArchivo});
        await newNota.save();
        res.redirect("/notas");
    }
});

router.put('/updateNota/:id', verificaToken, async function(req, res){
    let { titulo,  descripcion, user, refName } = req.body;
    if(!req.files){
        await Nota.findByIdAndUpdate(req.params.id, { title: titulo, description: descripcion, user: user });
        res.redirect("/notas");
    } else {
        let ref = req.files.ref;
        let nombreArchivoCortado = ref.name.split('.');
        let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
        let extencionesValidas = ["png", "jpg", "jpeg"];
        if (extencionesValidas.indexOf(extencion) < 0) {
            res.redirect("/notas");
        } else {
            let nombreArchivo = `${titulo}-${new Date().getMilliseconds()}.${extencion}`
            ref.mv(`public/assets/uploads/${nombreArchivo}`, (err) => {
                if (err) {
                    res.redirect("/notas");
                }
            });

            let pathImage = path.resolve(__dirname, `../public/assets/uploads/${refName}`);
            if(fs.existsSync(pathImage)){
                fs.unlinkSync(pathImage)
            }

            await Nota.findByIdAndUpdate(req.params.id, { title: titulo, description: descripcion, user: user, ref: nombreArchivo });
            res.redirect("/notas");
        }
    }
})

router.delete('/deleteNota/:id', verificaToken, async function(req, res) {
    let id = req.params.id;
    await Nota.findByIdAndDelete(id, function (err, docs) { 
        if (err){ 
            console.log(err);
            res.redirect("/notas"); 
        } 
        else{ 
            let pathImage = path.resolve(__dirname, `../public/assets/uploads/${docs.ref}`);
            if(fs.existsSync(pathImage)){
                fs.unlinkSync(pathImage)
            }
            res.redirect("/notas");
        } 
    }); 
    
});
module.exports = router