const jwt = require('jsonwebtoken');

/*
let verificaToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        if(err) {
            return res.status(401).json({
                ok: false,
                err,
            });
        }

        req.usuario = decoded.usuario;
        console.log(req.usuario)
        next();
    });
};
*/
let verificaToken = (req, res, next) => {
    let token = req.cookies.token;
    //let token = req.session.user;
    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        if(err) {
            console.log(err)
            res.clearCookie("token");
            res.redirect("/login");
        }
        req.user = decoded.usuario;
        //req.session.userData = decoded.usuario;
        next();
    });
};

module.exports = {
    verificaToken,
};