const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

let Schema = mongoose.Schema;

let rolesValidos = {
    values: [
        'ADMIN_ROLE',
        'USER_ROLE'
    ],
    message: '{VALUE} no es un rol válido'
}

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        unique: true,
        required: [true, "El nombre es requerido"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El correo es requerido"]
    },
    password: {
        type: String,
        required: [true, "El password es requerido"]
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true
    },
    img: {
        type: String,
        required: false
    },
    google: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
};
  
usuarioSchema.methods.matchPassword = async function(password) {
    return await bcrypt.compare(password, this.password);
};
  
  

module.exports = mongoose.model('Usuario', usuarioSchema)