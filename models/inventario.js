const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const NotaSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    ref: {
      type: String,
      required: false
    },
    user: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Nota", NotaSchema);