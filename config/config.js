//puerto
process.env.PORT = process.env.PORT || 3000

//entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

//token
//vencimiento del token
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30
//SEED de autenticacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'
//google
process.env.CLIENT_ID = process.env.CLIENT_ID || '1059296214694-234jn88l8t6jonp6cg78tsislck32mds.apps.googleusercontent.com'
//base de datos
let urlDB;
if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb://localhost:27017/lab07DB'
} else {
    urlDB = 'mongodb+srv://carlos_quispe:dcsOvRdpcaVa8ghW@cluster0-buskh.mongodb.net/lab08'
}
process.env.URLDB = urlDB